pipeline {
   agent none
   tools {
      // Maven Path
      maven "M2_HOME"
   }
   options {
       timeout(time: 1, unit: 'HOURS')
   }
  
  parameters {
    booleanParam(name: 'UNTTEST', defaultValue: true, description: 'Enable UnitTests ?')
    booleanParam(name: 'CODEANALYSIS', defaultValue: true, description: 'Enable CODE-ANALYSIS ?')
    }


   stages {
     stage('Checkout')
        {
        agent {
           label 'deploy'
           }
        steps { 
            git branch: 'master', url: 'https://gitlab.com/dotcom85/cicd-project.git'
        }
        } // end of checkout
        
    stage('Pre-check') 
        {
       agent {
            label 'deploy'
            }
        when {
            anyOf {
                changeset "App/**"
                changeset "Webapp/**"
            }
        }
        steps { 
            script {
                env.BUILDME = "yes" // Set env variable to enable further build stages
            }
            }
        
        } // End of PreCheck stage
    
    stage('Build Artifact') 
                {
                
                agent {
                    label 'deploy'
                    }
                    when {environment name: 'BUILDME', value: 'yes'}
                    steps {
                /* Check if unittest is enabled */
                script {
                    if (params.UNTTEST) {
                    unitstr = ""
                    } else {
                    unitstr = "-Dmaven.test.skip=true"
                    }

                echo "Building Jar Component ..."
                dir("./App") {
                sh "mvn clean package ${unitstr}"
                }

                echo "Building Jar Component ..."
                dir("./Webapp") {
                sh "mvn clean package"
                }
                }

            }
        
        } // End build artifact
        
        /* Running Code Coverage */

    stage('Code Coverage') {
        agent {
            label 'deploy'
        }
        when {
            allOf {
                expression { return params.CODEANALYSIS }
                environment name: 'BUILDME', value: 'yes'
            }
        }

        steps {
            echo "Running Code Coverage ..."
            dir("./App") {
            sh "mvn org.jacoco:jacoco-maven-plugin:0.8.3:prepare-agent"
            }
        }
    }  // End code coverage

    /* Running Sonarqube Analysis and publishing to Sonerqube Server*/
         stage('SonarQube Analysis') {
        agent { label 'deploy' }
        when {environment name: 'BUILDME', value: 'yes'}
        steps { 
         withSonarQubeEnv('mysonarqube') {
           dir ("./App") {
           sh 'mvn sonar:sonar'
              } 
        } 
        } 
        } // end SonarQube Analysis
    
    /* Add Quality Gate Check*/
            stage('Quality Gate')
               {
                steps { 
                script { 
                timeout (time: 1, unit: 'HOURS') { // Just in case something goes wrong, pipeline will be killed after a timeout.
                def qg = waitForQualityGate() // Re-use taskID previously collected by withSonerQubeEnv
                if (qg.status != 'OK') {
                    error "Pipeline aborted due to quality gate failure: ${qg.status}"
                }
                }
                }
                } 
            } // End Quality Gates
    
    stage('Upload Artifactory') 
            {          
            agent { label 'deploy' }
            when {environment name: 'BUILDME', value: 'yes'}
            steps {          
                script { 
                    /* Define the Artifactory Server details */
                    def server = Artifactory.server 'myartificatory'
                    def buildInfo = Artifactory.newBuildInfo()
                    buildInfo.env.capture = true
                    buildInfo.env.collect()
                    def uploadSpec = """{
                        "files": [{
                        "pattern": "Webapp/target/webapp.war", 
                        "target": "cicd"                   
                        }]
                    }"""
                    
                    /* Upload the war to  Artifactory repo */
                    /* server.upload(uploadSpec) */ // this upload war into Artifactory
                    server.upload spec: uploadSpec, buildInfo: buildInfo
                    // Publish build info.
                    server.publishBuildInfo buildInfo
        }
            }
            } // End Upload Artifact to Jfrog
        
        stage('Build Image') {
            agent {
                label 'deploy'
            }
            steps {
                script {
                docker.withRegistry('https://registry.hub.docker.com', 'dockerhub') {
                /* Build Docker Image locally */
                myImage = docker.build("dotcom85/product-image")

                /* Push the container to the Registry */
                myImage.push("${env.BUILD_NUMBER}")
                myImage.push("latest")
                /*
                echo "Deleting Old Images from Jenkins Server"
                sh 'docker image prune -af --filter "until=1h"' */

                }
                }
            }
            } // end build image
            
            stage('Deploy_To_Pilot')
            {
            agent {
            label 'pilot'
            }
            steps {
                git branch: 'master', url: 'https://gitlab.com/dotcom85/cicd-project.git'
                sh "mkdir sanity"
                step([$class: 'DockerComposeBuilder', dockerComposeFile: 'docker-compose.yml',
                option: [$class: 'StartAllServices'], useCustomDockerComposeFile: false])
            }
            } // end of Deploy to Pilot Machine
            
        stage('Smoke Test') {
        agent {
            label 'pilot'
        }
        steps {
            sh "sleep 10; chmod +x runsmokes.sh; ./runsmokes.sh"
        }
        } // Smoke test
        
    /* Deploy application to Kubernetes Cluster */

        stage('Deploy') {
            agent {
                label 'k8s'
            }
            steps {
                echo "Checkout Deployment Configuration File"
                git branch: 'master', url: 'https://gitlab.com/dotcom85/deploy.git'

                sh "kubectl apply -f ."
            }
        } // end k8s deploy
        
   } // end stages
} // end pipeline